###############################################################
SHELL = /bin/sh
VERSION = 1.0
###############################################################
CC = gcc -g
CFLAGS = -Wall -c -o
###############################################################
ifneq ($(wildcard depend.mk),)
	top_builddir = $(shell pwd)
else
	top_builddir = ../..
endif

top_srcdir 			= $(top_builddir)/src
monitor_server_dir	= $(top_srcdir)/monitor_server
monitor_client_dir	= $(top_srcdir)/monitor_client

include_dir			= $(top_srcdir)/include
readline_dir		= $(include_dir)/readline
netsnmp_dir			= $(include_dir)/net-snmp

lib_dir				= $(top_srcdir)/lib
object_dir			= $(top_srcdir)/object
hm_dir				= $(top_srcdir)/hm
log_dir				= $(top_srcdir)/log
conf_dir			= $(top_srcdir)/conf
vtysh_dir			= $(top_srcdir)/vtysh
vtysh_custom_dir	= $(vtysh_dir)/custom
###############################################################
