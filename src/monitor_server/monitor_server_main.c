/*
 * @Author: hanyanlong
 * @Date: 2023-12-25 16:49:37
 * @Description: 
 */
#include "monitor_server.h"
#include "sys_lib.h"
#include "conf.h"

extern int conf_init();

int main(int argc, char const *argv[])
{
    int ret;

    ret = conf_init();
    if(ret) {
        debug_exception("Failed in conf_init\n");
        goto OUT;
    }


OUT:
    return ret;
}
