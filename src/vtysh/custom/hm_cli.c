/*
 * @Author: hanyanlong
 * @Date: 2024-03-21 16:34:13
 * @Description: 
 */
#include "sys_lib.h"
#include "vtysh.h"
#include "command.h"
#include "hm.h"
#include "hm_vty.h"

DEFUN(healthcheck_monitor,
    healthcheck_monitor_cmd,
    "healthcheck NAME ("HM_TYPE")",
    "Healthcheck monitor\n"
    "The name of a healthcheck\n"
    "Monitor type "HM_TYPE_ICMP_STR"\n"
    "Monitor type "HM_TYPE_SNMP_STR"\n"
    "Monitor type "HM_TYPE_SAMBA_STR"\n")
{
    struct healthcheck_monitor_template *hm_template;

    if(strlen(argv[0]) >= MAX_NAME_LEN) {
        debug_err("  The name length is too long\n");
        return RET_ERR;
    }

    hm_template = malloc(sizeof(struct healthcheck_monitor_template));
    if(!hm_template) {
        debug_exception("Failed in hm template new\n");
        return RET_ERR;
    }

    strncpy(hm_template->name, argv[0], sizeof(hm_template->name));
    if(!strcmp(argv[1], HM_TYPE_ICMP_STR)) {
        hm_template->type = HM_TYPE_ICMP;
    } else if(!strcmp(argv[1], HM_TYPE_SNMP_STR)) {
        hm_template->type = HM_TYPE_SNMP;
    } else if(!strcmp(argv[1], HM_TYPE_SAMBA_STR)) {
        hm_template->type = HM_TYPE_SAMBA;
    }

    assign_vty(HEALTHCHECK_NODE, hm_template, hm_template->name);

    return RET_SUCCESS;
}

DEFUN(no_healthcheck_monitor,
    no_healthcheck_monitor_cmd,
    "no healthcheck NAME",
    NO_STR
    "Healthcheck monitor\n"
    "The name of a healthcheck\n")
{
    if(strlen(argv[0]) >= MAX_NAME_LEN) {
        debug_err("  The name length is too long\n");
        return RET_ERR;
    }

    return RET_SUCCESS;
}

struct cmd_node hm_node = {
    HEALTHCHECK_NODE,
    "%s(config-healthcheck-%s)# ",
    NULL
};

void healthcheck_cli_init()
{
    install_node(&hm_node);
    install_default(HEALTHCHECK_NODE);

    install_element(CONFIG_NODE, &healthcheck_monitor_cmd);
    install_element(CONFIG_NODE, &no_healthcheck_monitor_cmd);


}
