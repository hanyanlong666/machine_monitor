/*
 * @Author: hanyanlong
 * @Date: 2024-03-26 16:52:44
 * @Description: 
 */
#include "sys_lib.h"
#include "vtysh.h"
#include "command.h"
#include "monitor_object.h"
#include "object_vty.h"

DEFUN(monitor_obj,
    monitor_obj_cmd,
    "monitor-object NAME",
    "Monitor object\n"
    "The name of a monitor object\n")
{
    struct monitor_obj *mobj;

    if(strlen(argv[0]) >= MAX_NAME_LEN) {
        debug_err("  The name length is too long\n");
        return RET_ERR;
    }

    mobj = malloc(sizeof(struct monitor_obj));
    if(!mobj) {
        debug_exception("Failed in monitor obj new\n");
        return RET_ERR;
    }

    strncpy(mobj->name, argv[0], sizeof(mobj->name));

    assign_vty(MONITOR_OBJ_NODE, mobj, mobj->name);
    vty->index = mobj;

    return RET_SUCCESS;
}

DEFUN(no_monitor_obj,
    no_monitor_obj_cmd,
    "no monitor-object NAME",
    NO_STR
    "Monitor object\n"
    "The name of a monitor object\n")
{
    if(strlen(argv[0]) >= MAX_NAME_LEN) {
        debug_err("  The name length is too long\n");
        return RET_ERR;
    }

    return RET_SUCCESS;
}

struct cmd_node monitor_obj_node = {
    MONITOR_OBJ_NODE,
    "%s(config-monitor-object-%s)# ",
    NULL
};

void object_cli_init()
{
    install_node(&monitor_obj_node);
    install_default(MONITOR_OBJ_NODE);

    install_element(CONFIG_NODE, &monitor_obj_cmd);
    install_element(CONFIG_NODE, &no_monitor_obj_cmd);

}