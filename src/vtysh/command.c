/*
 * @Autor: hanyanlong
 * @Date: 2023-06-28 09:46:27
 * @Description: 
 */
// #include <readline/readline.h>
// #include <readline/history.h>
#include "sys_lib.h"
#include <fcntl.h>
#include <locale.h>
#include "readline/readline.h"
#include "readline/history.h"
#include "command.h"
#include "vtysh.h"

extern list vty_hash_table;
extern struct vty *vty;
extern int done;

/**
 * @description: 判断参数是否为多选项
 * @param {cmd_vector} *vector
 * @return {*}
 */
static int cmd_is_option(struct cmd_vector *vector)
{
    if(vector->next)
        return 1;

    return 0;
}

/**
 * @description: 判断参数是否为整数范围
 * @param {char} *cmd
 * @return {*}
 */
static int cmd_is_integer_range(struct cmd_vector *vector)
{
    char *cmd = vector->cmd;
    char *t;
    int hyphen_num = 0; // 连字符'-'数量

    if((cmd[0] != '<') || (cmd[strlen(cmd) - 1] != '>') || cmd[1] == '-' || cmd[strlen(cmd) - 2] == '-')
        return 0;

    for(t = cmd; *t; t++)
    {
        if(!isdigit(*t)) {
            if(*t == '-')
                hyphen_num++;
        }
    }

    if(hyphen_num == 1)
        return 1;

    return 0;
}

/**
 * @description: 判断参数是否为字符串参数
 * @param {char} *cmd
 * @return {*}
 */
static int cmd_is_string(struct cmd_vector *vector)
{
    char *cmd = vector->cmd;
    char *t;
    int i;

    for(t = cmd, i = 0; isupper(*t); t++, i++);
    if(i == strlen(cmd))
        return 1;

    return 0;
}

/**
 * @description: 判断参数是否为固定参数
 * @param {char} *cmd
 * @return {*}
 */
static int cmd_is_regular(struct cmd_vector *vector)
{
    char *cmd = vector->cmd;
    char *t;

    for(t = cmd; *t; t++)
    {
        if(!isalnum(*t))
        {
            if(*t != '-' && *t != '_')
                return 0;
        }
    }

    return 1;
}

/**
 * @description: 匹配选项参数
 * @param {char} *vector    vector
 * @param {char} *string    用户输入的cmd
 * @return {*}
 */
static inline int match_option(struct cmd_vector *vector, char *string)
{
    VECTOR_LOOP(vector)
    {
        if(!strcmp(vector->cmd, string))
            return 1;
    }

    return 0;
}

/**
 * @description: 匹配整数范围参数
 * @param {char} *vector    vector
 * @param {char} *string    用户输入的cmd
 * @return {*}
 */
static inline int match_integer_range(struct cmd_vector *vector, char *string)
{
    char *t;
    int min, max, value;
    int i;

    for(t = string, i = 0; isdigit(*t); t++, i++);
    if(i == strlen(string)) {
        min = atoi(vector->cmd + 1);
        max = atoi(strchr(vector->cmd, '-') + 1);
        value = atoi(string);

        if((value >= min) && (value <= max))
            return 1;
        // else
            // debug_out("The number is not within the legal range: %s\n", vector->cmd);
    }

    return 0;
}

/**
 * @description: 匹配字符串参数
 * @param {cmd_vector} *vector
 * @param {char} *string
 * @return {*}
 */
static inline int match_string(struct cmd_vector *vector, char *string)
{
    char *t;
    int i;

    // 检查是否都为可打印字符
    for(t = string, i = 0; isprint(*t); t++, i++);
    if(i == strlen(string))
        return 1;

    return 0;
}

/**
 * @description: 匹配固定参数
 * @param {cmd_vector} *vector
 * @param {char} *string
 * @return {*}
 */
static inline int match_regular(struct cmd_vector *vector, char *string)
{
    if(!strcmp(vector->cmd, string))
        return 1;
    return 0;
}

/**
 * @description: 获得命令元素每个vecter的命令的类型
 * @param {cmd_element} *cmd_element
 * @return {*}
 */
static int vector_cmd_type_get(struct cmd_element *cmd_element)
{
    list list = cmd_element->vector_list;
    listnode nn;
    struct cmd_vector *vector;

    LIST_LOOP(list, vector, nn)
    {
        /* check option must be first */
        if(cmd_is_option(vector))
        {
            while(vector)
            {
                vector->type = CMD_IS_OPTION;
                vector = vector->next;
            }
            continue;
        }
        else if(cmd_is_integer_range(vector))
        {
            vector->type = CMD_IS_INTEGER_RANGE;
            continue;
        }
        else if(cmd_is_string(vector))
        {
            vector->type = CMD_IS_STRING;
            continue;
        }
        else if(cmd_is_regular(vector))
        {
            vector->type = CMD_IS_REGULAR;
            continue;
        }
        else
        {
            debug_exception("Not supported vector command type.\n");
            return RET_ERR;
        }
    }

    return RET_SUCCESS;
}

static int match_cmd_type(struct cmd_vector *vector, char *string)
{
    switch(vector->type)
    {
        case CMD_IS_OPTION:
            return match_option(vector, string);
        case CMD_IS_INTEGER_RANGE:
            return match_integer_range(vector, string);
        case CMD_IS_STRING:
            return match_string(vector, string);
        case CMD_IS_REGULAR:
            return match_regular(vector, string);
        default:
            break;
    }

    return 0;
}

/**
 * @description: 匹配命令元素
 * @param {int} cmd_num
 * @param {char} *
 * @param {char} *
 * @param {int} *argc
 * @return {*}
 */
struct cmd_element *match_cmd_element(struct split_array *sa, struct split_array *vty_sa)
{
    list cmd_hash_table = vty->node->cmd_hash_table;
    list vector_list = NULL;
    listnode enode, vnode;
    struct cmd_element *cmd_element;
    struct cmd_vector *vector;
    int cmd_index;
    int matched;
    int ret;

    LIST_LOOP(cmd_hash_table, cmd_element, enode)
    {
        if(sa->size != cmd_element->size)
            continue;

        cmd_index = 0;
        vector_list = cmd_element->vector_list;

        LIST_LOOP(vector_list, vector, vnode)
        {
            matched = match_cmd_type(vector, sa->array[cmd_index]);
            if(matched)
            {
                /* 获取argc和*argv[] */
                if(vector->type != CMD_IS_REGULAR)
                {
                    ret = split_array_node_insert(vty_sa, sa->array[cmd_index]);
                    if(ret)
                    {
                        debug_err("Failed insert split line in array\n");
                        return NULL;
                    }
                }
                cmd_index++;
            }
        }

        if(cmd_index == sa->size)
            return cmd_element;
    }

    return NULL;
}

/**
 * @description: 处理命令
 * @param {char} *line
 * @return {*}
 */
int execute_func(char *line)
{
    struct cmd_element *cmd_element = NULL;
    struct split_array *sa, *vty_sa;
    int ret;

    if(strlen(line) >= MAX_VTY_COMMAND_LEN)
    {
        debug_err("Command is too long.\n");
        goto OUT;
    }

    sa = split_array_new();
    if(!sa)
    {
        debug_exception("Failed in split array new\n");
        ret = RET_ERR;
        goto OUT;
    }

    vty_sa = split_array_new();
    if(!vty_sa)
    {
        debug_exception("Failed in split array new\n");
        ret = RET_ERR;
        goto OUT;
    }

    // 分割命令
    ret = split_line_to_array(sa, line, " ");
    if(ret)
    {
        debug_err("Failed in split line to array\n");
        goto OUT;
    }

    cmd_element = match_cmd_element(sa, vty_sa);
    if(!cmd_element)
    {
        debug_out("[%s]: Unknown command.\n", line);
        goto OUT;
    }

    /* call the function. */
    ret = cmd_element->func(vty, vty_sa->size, vty_sa->array);

OUT:
    split_array_delete(sa);
    split_array_delete(vty_sa);

    return ret;
}

/**
 * @description: 按下 ? 打印帮助信息
 * @return {*}
 */
int vtysh_describe()
{
    list cmd_hash_table = vty->node->cmd_hash_table;
    list vector_list;
    listnode enode, vnode;
    struct cmd_element *cmd_element;
    struct cmd_vector *vector;
    struct split_array *line_sa;
    int event_type;
    char *cur_cmd;
    int cur_cmd_len;
    int cmd_index;
    int matched;
    int found = 0, ending = 0;
    int ret;

    line_sa = split_array_new();
    if(!line_sa) {
        debug_exception("Failed in split array new\n");
        ret = RET_ERR;
        goto OUT;
    }

    ret = split_line_to_array(line_sa, rl_line_buffer, " ");
    if(ret)
    {
        debug_err("Failed in split line to array\n");
        ret = RET_ERR;
        goto OUT;
    }

    event_type = rl_line_buffer[strlen(rl_line_buffer) - 1] == ' ' ? DESC_EVENT_NEXT_GET : DESC_EVENT_CUR_GET;

    LIST_LOOP(cmd_hash_table, cmd_element, enode)
    {
        /* 数量不符合 pass */
        if(line_sa->size > cmd_element->size)
        continue;

        vector_list = cmd_element->vector_list;
        cmd_index = 0;

        LIST_LOOP(vector_list, vector, vnode)
        {
            if(cmd_index >= line_sa->size)
                break;

            cur_cmd = line_sa->array[cmd_index];
            cur_cmd_len = strlen(cur_cmd);
            matched = 0;

            /* 根据命令类型选择匹配方式 */
            switch(vector->type)
            {
                case CMD_IS_OPTION:
                    VECTOR_LOOP(vector)
                    {
                        if(!strncmp(vector->cmd, cur_cmd, cur_cmd_len))
                        {
                            matched = 1;
                            cmd_index++;
                            break;
                        }
                    } 
                    break;
                case CMD_IS_INTEGER_RANGE:
                    ret = match_integer_range(vector, cur_cmd);
                    if(ret)
                    {
                        matched = 1;
                        cmd_index++;
                    }
                    break;  
                case CMD_IS_STRING:
                    ret = match_string(vector, cur_cmd);
                    if(ret)
                    {
                        matched = 1;
                        cmd_index++;
                    }
                    break;
                case CMD_IS_REGULAR:
                    if(!strncmp(vector->cmd, cur_cmd, cur_cmd_len))
                    {
                        matched = 1;
                        cmd_index++;
                    }
                    break;
                default:
                    ret = RET_ERR;
                    goto OUT;
            }

            /* 未能匹配到当前命令元素 尝试匹配下一个 */
            if(!matched)
                break;

            /* 匹配成功 返回命令 */
            if(cmd_index == line_sa->size)
            {
                if(event_type == DESC_EVENT_CUR_GET)
                {
                    if(!found)
                    {
                        debug_out("\n");
                        found = 1;
                    }
                    DEBUG_CMD_DESC(vector->cmd, vector->desc);
                }
                else
                {
                    if(cmd_element->size == 1)
                    {
                        ending = 1;
                        break;
                    }

                    /* 提示出下一个节点的命令 */
                    nextnode(vnode);
                    if(!vnode)
                        break;

                    vector = getdata(vnode);
                    VECTOR_LOOP(vector)
                    {
                        if(!found)
                        {
                            debug_out("\n");
                            found = 1;
                        }
                        DEBUG_CMD_DESC(vector->cmd, vector->desc);
                    }  
                }
            }
        }
    }

    ret = RET_SUCCESS;

OUT:
    if(ret == RET_SUCCESS)
    {
        if(!found)
        {
            debug_out("\n");
            if(ending)
            {
                DEBUG_CMD_DESC("* Reached the end of the command.", "");
            }
            else
            {
                DEBUG_CMD_DESC("* There is no matched command.", "");
            }
        }
    }

    split_array_delete(line_sa);
    rl_on_new_line();

    return ret;
}

/**
 * @description: 按下 TAB 处理函数
 * @param {char} *text
 * @param {int} state
 * @return {*}
 */
char *command_generator(const char *text, int state)
{
    static struct split_array *match_sa;
    static int match_index;

    /* first call this function */
    if(!state)
    {
        list cmd_hash_table = vty->node->cmd_hash_table;
        list vector_list;
        listnode enode, vnode;
        struct cmd_element *cmd_element;
        struct cmd_vector *vector;
        struct split_array *line_sa;
        int event_type;
        char *cur_cmd;
        int cur_cmd_len;
        int cmd_index;
        int matched;
        int ret;

        /* 没有内容 pass */
        if(!strlen(text) && !rl_point)
            return NULL;

        /**
         * 首先分割当前命令行输入的内容
         * 并重置所有此函数用到的全局变量和静态变量
         */
        if(match_sa)
        {
            split_array_reset(match_sa);
        }
        else
        {
            match_sa = split_array_new();
            if(!match_sa)
            {
                debug_exception("Failed split array new\n");
                return NULL;
            } 
        }
        match_index = 0;

        line_sa = split_array_new();
        if(!line_sa) {
            debug_exception("Failed split array new\n");
            return NULL;
        }

        ret = split_line_to_array(line_sa, rl_line_buffer, " ");
        if(ret)
        {
            debug_exception("Failed split line to array\n");
            split_array_delete(line_sa);
            return NULL;
        }

        event_type = rl_line_buffer[strlen(rl_line_buffer) - 1] == ' ' ? TAB_EVENT_NEXT_GET : TAB_EVENT_COMPLETION;

        LIST_LOOP(cmd_hash_table, cmd_element, enode)
        {
            /* 数量不符合 pass */
            if(line_sa->size > cmd_element->size)
            continue;

            vector_list = cmd_element->vector_list;
            cmd_index = 0;

            LIST_LOOP(vector_list, vector, vnode)
            {
                if(cmd_index >= line_sa->size)
                    break;

                cur_cmd = line_sa->array[cmd_index];
                cur_cmd_len = strlen(cur_cmd);
                matched = 0;

                /* 根据命令类型选择匹配方式 */
                switch(vector->type)
                {
                    case CMD_IS_OPTION:
                        VECTOR_LOOP(vector)
                        {
                            if(!strncmp(vector->cmd, cur_cmd, cur_cmd_len))
                            {
                                matched = 1;
                                cmd_index++;
                                break;
                            }
                        } 
                        break;
                    case CMD_IS_INTEGER_RANGE:
                        ret = match_integer_range(vector, cur_cmd);
                        if(ret)
                        {
                            matched = 1;
                            cmd_index++;
                        }
                        break;  
                    case CMD_IS_STRING:
                        ret = match_string(vector, cur_cmd);
                        if(ret)
                        {
                            matched = 1;
                            cmd_index++;
                        }
                        break;
                    case CMD_IS_REGULAR:
                        if(!strncmp(vector->cmd, cur_cmd, cur_cmd_len))
                        {
                            matched = 1;
                            cmd_index++;
                        }
                        break;
                    default:
                        split_array_delete(line_sa);
                        return NULL;
                }

                /* 未能匹配到当前命令元素 尝试匹配下一个 */
                if(!matched)
                    break;

                /* 匹配成功 返回命令 */
                if(cmd_index == line_sa->size)
                {
                    if(event_type == TAB_EVENT_COMPLETION)
                    {
                        ret = split_array_node_insert(match_sa, vector->cmd);
                        if(ret)
                        {
                            debug_exception("Failed insert split line in array\n");
                            split_array_delete(line_sa);
                            return NULL;
                        }
                    }
                    else
                    {
                        /* 提示出下一个节点的命令 */
                        nextnode(vnode);
                        if(!vnode)
                            break;

                        vector = getdata(vnode);
                        if(vector->type == CMD_IS_OPTION || vector->type == CMD_IS_REGULAR)
                        {
                            VECTOR_LOOP(vector)
                            {
                                ret = split_array_node_insert(match_sa, vector->cmd);
                                if(ret)
                                {
                                    debug_err("Failed insert split line in array\n");
                                    split_array_delete(line_sa);
                                    return NULL;
                                }
                            }  
                        }
                    }
                }
            }
        }

        /* 使用完立即释放 */
        split_array_delete(line_sa);
    }

    if(match_index < match_sa->size)
        return strdup(match_sa->array[match_index++]);

    return NULL;
}

/* Attempt to complete on the contents of TEXT.  START and END bound the
   region of rl_line_buffer that contains the word to complete.  TEXT is
   the word to complete.  We can use the entire contents of rl_line_buffer
   in case we want to do some simple parsing.  Return the array of matches,
   or NULL if there aren't any. */
/**
 * @description: 
 * @param {char} *text
 * @param {int} start
 * @param {int} end
 * @return {*}
 */
char **vtysh_completion(const char *text, int start, int end)
{
    char **matches = NULL;

    if(VTY_NODE_TYPE == AUTH_NODE)
        return NULL;

    rl_completion_append_character = '\0';

	matches = rl_completion_matches (text, (rl_compentry_func_t *)command_generator);

	if(matches)
		rl_point = rl_end;

    return matches;
}

/**
 * @description: 根据节点类型找到对应命令节点
 * @param {enum node_type} ntype
 * @return {*}
 */
struct cmd_node *cmd_node_get(enum node_type ntype)
{
    listnode listnode;
    struct cmd_node *cmd_node;

    LIST_LOOP(vty_hash_table, cmd_node, listnode)
    {
        if(cmd_node->ntype == ntype)
            return cmd_node;
    }

    return NULL;
}

static int cmd_hash_list_cmp(struct cmd_element *e1, struct cmd_element *e2)
{
    return strcmp(e1->string, e2->string);
}

int install_node(struct cmd_node *cmd_node)
{
    struct cmd_node *new_ndoe;
    new_ndoe = malloc(sizeof(struct cmd_node));
    memset(new_ndoe, 0, sizeof(struct cmd_node));
    if(!new_ndoe)
    {
        debug_exception("Failed in malloc new_ndoe, ntype = %d\n", cmd_node->ntype);
        return RET_ERR;
    }

    new_ndoe->ntype = cmd_node->ntype;
    new_ndoe->prompt = cmd_node->prompt;
    new_ndoe->cmd_hash_table = list_new();
    new_ndoe->cmd_hash_table->cmp = (void *)cmd_hash_list_cmp;

    listnode_add_sort(vty_hash_table, new_ndoe);

    return RET_SUCCESS;
}

/**
 * @description: 初始化一个 cmd vector
 * @return {*}
 */
struct cmd_vector *cmd_vector_new()
{
    struct cmd_vector *vector;
    vector = malloc(sizeof(struct cmd_vector));
    memset(vector, 0, sizeof(struct cmd_vector));
    if(!vector)
    {
        debug_exception("Failed in malloc vector\n");
        return NULL;
    }

    return vector;
}

void cmd_vector_delete(struct cmd_vector *cmd_vector)
{
    struct cmd_vector *del_vector;

    while(cmd_vector)
    {
        del_vector = cmd_vector;
        cmd_vector = cmd_vector->next;
        free(del_vector->cmd);
        free(del_vector->desc);
        free(del_vector);
    }
}

/**
 * @description: 遍历打印vector链表
 * @param {list} list
 * @return {*}
 */
void loop_vector_list_print(list list)
{
    listnode nn;
    struct cmd_vector *vector;
    LIST_LOOP(list, vector, nn)
    {
        VECTOR_LOOP(vector)
        {
            debug_out("type = %d [ %-32s%s ]\n", vector->type, vector->cmd, vector->desc);
        }
    }
}

void loop_cmd_element_print(struct cmd_element *cmd_element)
{
    if(!cmd_element)
        return;

    debug_out("string: %s, size: %d\n", cmd_element->string, cmd_element->size);
    loop_vector_list_print(cmd_element->vector_list);
    debug_out("\n");
}

void loop_cmd_hash_table_print(list cmd_hash_table)
{
    struct cmd_element *cmd_element;
    listnode nn;

    if(!cmd_hash_table)
        return;

    LIST_LOOP(cmd_hash_table, cmd_element, nn)
    {
        debug_out("  %s\n", cmd_element->string);
    }
}

/**
 * @description: 依次返回解析到的帮助信息
 * @param {char} *
 * @return {*}
 */
char *make_vector_desc(char **string)
{
	char *cp, *start, *token;
	int len;

	cp = *string;

	if (cp == NULL)
		return NULL;

	/* Skip white spaces. */
	while (isspace((int) *cp) && *cp != '\0' && *cp != '\t')
		cp++;

	/* Return if there is only white spaces */
	if (*cp == '\0')
		return NULL;

	start = cp;

	while (!(*cp == '\r' || *cp == '\n') && *cp != '\0')
		cp++;

	len = cp - start;

    token = malloc(len + 1);
    memcpy(token, start, len);
    *(token + len) = '\0';

	*string = cp;

	return token;
}

/**
 * @description: 解析命令和帮助指针 并挂到对应命令元素的 vector list上
 * @param {cmd_element} *cmd_element
 * @return {*}
 */
int command_make_cmd_vector(struct cmd_element *cmd_element)
{
    struct cmd_vector *vector = NULL;
	int multiple = 0, cmd_index = 0;;
	char *sp;
	int len;
	char *cp;
    char *dp;
    int ret = RET_SUCCESS;

    cmd_element->vector_list = list_new();

	cp = cmd_element->string;
    dp = cmd_element->doc;

	while(1)
	{
		while(isspace((int) *cp) && *cp != '\0')
			cp++;

		if(*cp == '(')
        {
			multiple = 1;
			cp++;
		}

		if(*cp == ')')
        {
			multiple = 0;
            cmd_index = 0;
			cp++;
		}

		if(*cp == '|')
        {
			if (!multiple)
            {
                debug_exception("Command parse error! cmd_element->string = %s\n", cmd_element->string);
                return RET_ERR;
			}
            cmd_index++;
			cp++;
		}

		while(isspace((int) *cp) && *cp != '\0')
			cp++;

		if(*cp == '(')
        {
			multiple = 1;
			cp++;
		}

		if(*cp == '\0')
        {
            cmd_element->size = cmd_element->vector_list->count;
            ret = vector_cmd_type_get(cmd_element);
			return ret;
        }

		sp = cp;

		while(!(isspace((int) *cp) || *cp == '\r' || *cp == '\n' || *cp == ')' || *cp == '|') && *cp != '\0')
			cp++;

		len = cp - sp;

        if(multiple && cmd_index)
        {
            struct cmd_vector *new_vector;
            new_vector = cmd_vector_new();
            new_vector->cmd = malloc(len + 1);
            memcpy(new_vector->cmd, sp, len);
            *(new_vector->cmd + len) = '\0';

            vector->next = new_vector;
            vector = new_vector;
        }
        else
        {
            vector = cmd_vector_new();
            vector->cmd = malloc(len + 1);
            memcpy(vector->cmd, sp, len);
            *(vector->cmd + len) = '\0';

            listnode_add(cmd_element->vector_list, vector);
        }

        vector->desc = make_vector_desc(&dp);
    }
}

/**
 * @description: 注册命令元素
 * @param {enum node_type} ntype
 * @param {cmd_element} *cmd_element
 * @return {*}
 */
int install_element(enum node_type ntype, struct cmd_element *cmd_element)
{
    struct cmd_node *cmd_node;
    struct cmd_element *new_element;
    int ret;

    if(strlen(cmd_element->string) >= MAX_VTY_COMMAND_LEN)
    {
        debug_exception("cmd_element->string too long\n");
        return RET_ERR;
    }
    if(strlen(cmd_element->doc) >= MAX_VTY_DOC_LEN)
    {
        debug_exception("cmd_element->doc too long\n");
        return RET_ERR;
    }

    cmd_node = cmd_node_get(ntype);
    if(!cmd_node)
    {
        // 后续会加 get ntype str 的方法
        debug_exception("cmd_node is NULL, ntype = %d\n", ntype);
        return RET_ERR;
    }

    new_element = malloc(sizeof(struct cmd_element));
    memset(new_element, 0, sizeof(struct cmd_element));
    if(!new_element)
    {
        debug_exception("Failed in malloc new_element, cmd_element->string = %s\n", cmd_element->string);
        return RET_ERR;
    }
    new_element->string = cmd_element->string;
    new_element->doc = cmd_element->doc;
    new_element->func = cmd_element->func;

    // 填充 cmd vector
    ret = command_make_cmd_vector(new_element);
    if(ret) {
        debug_exception("Failed in command_make_cmd_vector\n");
        return RET_ERR;
    }

    listnode_add_sort(cmd_node->cmd_hash_table, new_element);

    return RET_SUCCESS;
}

/***************** default cli start *****************/
DEFUN(list_func,
    list_cmd,
    "list",
    "Display all commands for the current node\n")
{
    loop_cmd_hash_table_print(vty->node->cmd_hash_table);
    return RET_SUCCESS;
}
#if 0
DEFUN(goback_func,
    goback_cmd,
    "goback",
    "Go back to the previous node\n")
{
    if(vty->prev_node)
        assign_vty(vty->prev_node->ntype);
    else
        debug_out("  There is currently no previous node.\n");

    return RET_SUCCESS;
}
#endif
DEFUN(exit_func,
    exit_cmd,
    "exit",
    "Exit current node\n")
{
    int parent_ntype = UNSPEC_NODE;

    switch(VTY_NODE_TYPE)
    {
        case AUTH_NODE:
            break;
        case ENABLE_NODE:
            done = VTY_QUIT;
            debug_out("The program will quit now.\n");
            break;
        case CONFIG_NODE:
            parent_ntype = ENABLE_NODE;
            break;
        case MONITOR_OBJ_NODE:
        case HEALTHCHECK_NODE:
        case LOG_NODE:
            parent_ntype = CONFIG_NODE;
            break;
        default:
            break;
    }

    if(parent_ntype != UNSPEC_NODE)
        assign_vty(parent_ntype, NULL, NULL);

    return RET_SUCCESS;
}

DEFUN(quit_func,
    quit_cmd,
    "quit",
    "Quit program\n")
{
    done = VTY_QUIT;
    debug_out("The program will quit now.\n");

    return RET_SUCCESS;
}

/**
 * @description: 在节点下注册默认命令
 * @param {enum node_type} ntype
 * @return {*}
 */
int install_default(enum node_type ntype)
{
    install_element(ntype, &list_cmd);
    // install_element(ntype, &goback_cmd); 存在安全问题，先注释掉
    install_element(ntype, &exit_cmd);
    install_element(ntype, &quit_cmd);

    return RET_SUCCESS;
}
/***************** default cli end *****************/

/***************** base cli start *****************/
DEFUN(configure_terminal,
    configure_terminal_cmd,
    "configure terminal",
    "Configuration from vty interface\n"
    "Configuration terminal\n")
{
    assign_vty(CONFIG_NODE, NULL, NULL);

    return RET_SUCCESS;
}

struct cmd_node enable_node = {
    ENABLE_NODE,
    "%s# ",
    NULL
};

struct cmd_node config_node = {
    CONFIG_NODE,
    "%s(config)# ",
    NULL
};

/**
 * @description: 初始化基础节点及命令
 * @return {*}
 */
void base_cli_init()
{
    install_node(&enable_node);
    install_node(&config_node);

    install_default(ENABLE_NODE);
    install_default(CONFIG_NODE);

    install_element(ENABLE_NODE, &configure_terminal_cmd);
}
/***************** base cli end *****************/
