/*
 * @Autor: hanyanlong
 * @Date: 2023-06-27 17:14:58
 * @Description: 
 */
#include "sys_lib.h"

extern void base_cli_init();
extern void object_cli_init();
extern void healthcheck_cli_init();

void vtysh_cmd_init()
{
    base_cli_init();
    object_cli_init();
    healthcheck_cli_init();
}