/*
 * @Autor: hanyanlong
 * @Date: 2023-06-28 09:47:00
 * @Description: 
 */
#ifndef __VTY_H__
#define __VTY_H__

#define MAX_VTY_READ_BUFSIZE    1024
#define MAX_VTY_COMMAND_LEN     800
#define MAX_VTY_DOC_LEN         800

#define CMD_ARGC_MAX            MAX_ARRAY_ARGC
#define VTY_COMMAND_PARAM_LEN   MAX_ARRAY_PARAM_LEN

#define INVALID_OPTION_STR  "Invalid option, please re-enter"
#define SHOW_STR    "Show\n"
#define NO_STR      "No\n"

#define VTY_NODE_TYPE vty->node->ntype 

enum node_type {
    UNSPEC_NODE,
    AUTH_NODE,          // 登录认证节点
    ENABLE_NODE,        // 登录后的初始节点
    CONFIG_NODE,        // 配置模块节点
    MONITOR_OBJ_NODE,   // 监控对象节点
    HEALTHCHECK_NODE,   // 健康检查节点
    LOG_NODE,           // 日志节点

    MAX_VTY_NODE,
};

enum tab_event_type {
    TAB_EVENT_COMPLETION,   // 尝试补齐
    TAB_EVENT_NEXT_GET,     // 尝试获取下一个vector的命令
};

enum describe_event_type {
    DESC_EVENT_CUR_GET,     // 尝试获取当前命令的帮助
    DESC_EVENT_NEXT_GET,    // 尝试获取下一个vector的帮助
};

struct cmd_node {
    enum node_type ntype;
    char *prompt;
    list cmd_hash_table;  // cmd_element
};

// maybe should pun in vtysh.h
struct vty {
    struct cmd_node *node;      // 当前节点
    struct cmd_node *prev_node; // 上一节点
    void *index;                // 暂存数据用
    char *index_name;           // 配置的名称
};

struct cmd_element {
    char *string;
    int (*func)(struct vty *, int, char **);
    char *doc;
    list vector_list;
    int size;
};

struct cmd_vector {
    char *cmd;
    char *desc;
    int type; // vector_cmd_type
    struct cmd_vector *next;
};

enum vector_cmd_type {
    CMD_IS_OPTION = 1,
    CMD_IS_INTEGER_RANGE,
    CMD_IS_STRING,
    CMD_IS_REGULAR,
    CMD_TYPE_MAX,
};

/**************** function start ****************/
int install_node(struct cmd_node *cmd_node);
int install_element(enum node_type ntype, struct cmd_element *cmd_element);
int install_default(enum node_type ntype);

char *stripwhite(char *string);
struct cmd_node *cmd_node_get(enum node_type ntype);
void cmd_vector_delete(struct cmd_vector *cmd_vector);

void loop_vector_list_print(list list);
void loop_cmd_element_print(struct cmd_element *cmd_element);
void loop_cmd_hash_table_print(list cmd_hash_table);

/**************** function end ****************/

/**************** macro start ****************/
/* DEFUN for vty command interafce. Little bit hacky */
#define DEFUN(funcname, cmdname, cmdstr, helpstr)\
    int funcname(struct vty *, int, char **);\
    struct cmd_element cmdname =\
    {\
        cmdstr,\
        funcname,\
        helpstr,\
        NULL,\
        0,\
    };\
    int funcname\
    (struct vty *vty, int argc, char **argv)

#define VECTOR_LOOP(v) for(; v; v = v->next)

#define DEBUG_CMD_DESC(cmd, desc) debug_out("  %-32s%s\n", cmd, desc)

/**************** macro end ****************/


#endif