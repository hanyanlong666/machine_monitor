/*
 * @Autor: hanyanlong
 * @Date: 2023-06-20 14:50:47
 * @Description: 
 */
#ifndef __VTYSH_H__
#define __VTYSH_H__

#define WORK_SPACE ".vtysh"

#define MAX_PROMPT_LEN 32
#define VTY_QUIT 1

#ifdef CONFIG_MONITOR_SERVER
int vtysh_in_from_file(FILE *fp);
#endif
#ifdef CONFIG_MONITOR_CLIENT
int vtysh_in();
#endif

void destroy_vtysh();
void assign_vty(int ntype, void *data, char *dname);

#endif