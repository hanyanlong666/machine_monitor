/*
 * @Autor: hanyanlong
 * @Date: 2023-06-20 14:50:47
 * @Description: 用户控制模块
 */
// #include <readline/readline.h>
// #include <readline/history.h>
#include "sys_lib.h"
#include <fcntl.h>
#include <locale.h>
#include "readline/readline.h"
#include "readline/history.h"
#include "command.h"
#include "vtysh.h"

extern int vtysh_cmd_init();

list vty_hash_table;
struct vty *vty;

// 非零时，此全局变量表示用户已使用此程序
int done;
char username[MAX_NAME_LEN];

/***************** vtysh init and free start *****************/
/**
 * @description: 初始化vty
 * @return {*}
 */
int vty_init()
{
    vty = malloc(sizeof(struct vty));
    memset(vty, 0, sizeof(struct vty));
    if(!vty)
    {
        debug_exception("Failed malloc vty\n");
        return RET_ERR;
    }
    memset(vty, 0, sizeof(struct vty));

    vty->node = cmd_node_get(ENABLE_NODE);
    vty->prev_node = NULL;
    vty->index = NULL;
    vty->index_name = NULL;

    return RET_SUCCESS;
}

void vty_free()
{
    if(!vty)
        return;

    if(vty->index)
    {
        free(vty->index);
        if(vty->index_name)
            free(vty->index_name);
    }
    free(vty);
}

static int vty_hash_list_cmp(struct cmd_node *n1, struct cmd_node *n2)
{
    return (n1->ntype < n2->ntype) ? -1 : 0;
}

/**
 * @description: 初始化vty链表
 * @return {*}
 */
void vty_hash_table_init()
{
    vty_hash_table = list_new();
    vty_hash_table->cmp = (void *)vty_hash_list_cmp;
}

/**
 * @description: 释放vty链表
 * @return {*}
 */
void vty_hash_list_free()
{
    struct cmd_node *cmd_node;
    struct cmd_element *cmd_element;
    struct cmd_vector *cmd_vector;
    listnode nnode, enode, vnode;

    if(!vty_hash_table)
        return;

    LIST_LOOP(vty_hash_table, cmd_node, nnode)
    {
        LIST_LOOP(cmd_node->cmd_hash_table, cmd_element, enode)
        {
            LIST_LOOP(cmd_element->vector_list, cmd_vector, vnode)
            {
                cmd_vector_delete(cmd_vector);
            }

            list_delete(cmd_element->vector_list);
            free(cmd_element);
        }

        list_delete(cmd_node->cmd_hash_table);
        free(cmd_node);
    }

    list_delete(vty_hash_table);
}

/**
 * @description: 程序退出前释放全部申请的内存
 * @return {*}
 */
void destroy_vtysh()
{
    vty_free();
    vty_hash_list_free();
}

// 初始化vtysh
int vtysh_init()
{
    int ret;

    vty_hash_table_init();

    ret = vtysh_cmd_init();
    if(ret) {
        debug_exception("Failed initing vtysh cmd\n");
        goto OUT;
    }

    ret = vty_init();
    if(ret) {
        debug_exception("Failed initing vty\n");
       goto OUT;
    }

OUT:
    return ret;
}
/***************** vtysh init and free end *****************/

/***************** vtysh execute start *****************/
extern char **vtysh_completion(const char *text, int start, int end);
extern int vtysh_describe();
extern int execute_func(char *line);

int username_get()
{
    FILE *fp = NULL;
    char cmd[MAX_COMMAND_LEN] = "";
    char buf[MAX_NAME_LEN];

    snprintf(cmd, sizeof(cmd) - 1, "whoami");
    fp = popen(cmd, "r");
    if(!fp) {
        debug_exception("Failed popen cmd: %s\n", cmd);
        return RET_ERR;
    }

    fgets(buf, sizeof(buf), fp);
    memset(username, 0, sizeof(username));
    strcpy(username, string_useless_del(buf));

    pclose(fp);

    return RET_SUCCESS;
}

char *vtysh_prompt()
{
    static char buf[(MAX_NAME_LEN * 2) + MAX_PROMPT_LEN]; 

    if(vty->index_name)
        snprintf(buf, sizeof(buf) - 1, vty->node->prompt, username, vty->index_name);
    else
        snprintf(buf, sizeof(buf) - 1, vty->node->prompt, username);

    return buf;
}

/**
 * @description: 切换VTY节点
 * @param {int} ntype
 * @return {*}
 */
void assign_vty(int ntype, void *data, char *dname)
{
    if(vty->index)
    {
        free(vty->index);
        vty->index = NULL;

        if(vty->index_name)
        {
            free(vty->index_name);
            vty->index_name = NULL;
        }
    }

    vty->prev_node = vty->node;
    vty->node = cmd_node_get(ntype);
    if(data)
    {
        vty->index = data;
        if(dname)
            vty->index_name = strdup(dname);
    }
}

/**
 * @description: 切换当前工作目录
 * @return {*}
 */
void change_workspace()
{
    mkdir(WORK_SPACE, 0777);
    chdir(WORK_SPACE);
}

/**
 * @description: 退出程序前删除工作目录
 * @return {*}
 */
void destroy_workspace()
{
    char cmd[MAX_COMMAND_LEN] = "";
    snprintf(cmd, sizeof(cmd) - 1, "rm -rf "WORK_SPACE"");
    chdir("..");
    system(cmd);
}

/* Tell the GNU Readline library how to complete.  We want to try to complete
   on command names if this is the first word in the line, or on filenames
   if not. */
void initialize_readline()
{
    #define VSOS_CMD_HISTORY_MAX 10
    stifle_history(VSOS_CMD_HISTORY_MAX);

    /* Allow conditional parsing of the ~/.inputrc file. */
    rl_readline_name = "MM";

    /* Tell the completer that we want a crack first. */
    rl_attempted_completion_function = (rl_completion_func_t *)vtysh_completion;

    rl_bind_key('?', vtysh_describe);
}

/**
 * @description: vtysh入口
 * @return {*}
 */
int vtysh_in()
{
    char *line, *s;
    int ret = 0;

    change_workspace();
    username_get();

    setlocale(LC_ALL, "");

    // Bind our completer
    initialize_readline();

    ret = vtysh_init();
    if(ret) {
        debug_exception("Failed init vtysh!\n");
        return RET_ERR;
    }

    debug_out("\nHi, I'm MM.\n\n");

    // 循环读取和执行，直到用户退出
    for( ; done != VTY_QUIT; )
    {
        line = readline(vtysh_prompt());
        if(!line)
            break;

        // 删除该行的前导和尾部空白. 如果还有剩余内容, 请将其添加到历史记录列表中并执行它
        s = stripwhite(line);

        if(*s)
        {
            add_history(s);
            ret = execute_func(s);
        }

        free(line);
    }

    destroy_workspace();

    return ret;
}

int vtysh_in_from_file(FILE *fp)
{
    char line[MAX_DATA_LEN] = "";
    char *s;
    int ret = 0;

    if(!fp) {
        debug_exception("fp is NULL!\n");
        return RET_ERR;
    }

    change_workspace();

    // 循环读取和执行，直到读取完文件
    while((fgets(line, sizeof(line), fp)) && (done != VTY_QUIT) )
    {
        // 删除该行的前导和尾部空白. 如果还有剩余内容, 请将其添加到历史记录列表中并执行它
        s = stripwhite(line);

        if(*s)
            ret = execute_func(s);

        memset(line, 0, sizeof(line));
    }

    destroy_workspace();

    return ret;
}
/***************** vtysh execute end *****************/
