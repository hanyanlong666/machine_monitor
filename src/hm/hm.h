/*
 * @Author: hanyanlong
 * @Date: 2024-03-21 16:35:57
 * @Description: 
 */
#ifndef __HM_H__
#define __HM_H__

#define MAX_COMMUNITY_LEN 64

#ifndef SNMP_VERSION_1
#define SNMP_VERSION_1	   0
#endif
#ifndef SNMP_VERSION_2c
#define SNMP_VERSION_2c    1
#endif
#ifndef SNMP_VERSION_2u
#define SNMP_VERSION_2u    2    /* not (will never be) supported by this code */
#endif
#ifndef SNMP_VERSION_3
#define SNMP_VERSION_3     3
#endif

#define HM_TYPE_ICMP_STR    "icmp"
#define HM_TYPE_SNMP_STR    "snmp"
#define HM_TYPE_SAMBA_STR   "samba"

#define HM_TYPE HM_TYPE_ICMP_STR\
                "|"HM_TYPE_SNMP_STR""\
                "|"HM_TYPE_SAMBA_STR""

enum healthcheck_monitor_type {
    HM_TYPE_ICMP = 1,
    HM_TYPE_SNMP,
    HM_TYPE_SAMBA,
};

struct monitor_icmp {

};

struct monitor_snmp {
    int version;
    char community[MAX_COMMUNITY_LEN];
};

struct monitor_samba {

};

struct healthcheck_monitor_template {
    char name[MAX_NAME_LEN];
    int type;
    union {
        struct monitor_icmp icmp;
        struct monitor_snmp snmp;
        struct monitor_samba samba;
    } monitor;

    int ref;
};

int check_icmp();
int check_snmp();
int check_samba();

#endif