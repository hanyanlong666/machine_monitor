/*
 * @Author: hanyanlong
 * @Date: 2024-03-25 10:23:57
 * @Description: 
 */
#include "sys_lib.h"
#include "hm.h"
#include "hm_vty.h"

static inline struct healthcheck_monitor_template *hm_template_new()
{
    struct healthcheck_monitor_template *hm_template;
    hm_template = malloc(sizeof(struct healthcheck_monitor_template));
    memset(hm_template, 0, sizeof(struct healthcheck_monitor_template));
    if(!hm_template) {
        debug_exception("Failed malloc hm_template\n");
        return NULL;
    }
    return hm_template;
}

static inline void hm_template_delete(struct healthcheck_monitor_template *hm_template)
{
    if(!hm_template)
        return;
    free(hm_template);
}

