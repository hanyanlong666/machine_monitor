/*
 * @Author: hanyanlong
 * @Date: 2023-12-25 17:17:09
 * @Description: 
 */
#include "monitor_client.h"
#include "sys_lib.h"
#include "vtysh.h"

int main(int argc, char const *argv[])
{
    int ret;

    ret = vtysh_in();
    if(ret) {
        debug_exception("Failed in vtysh_in\n");
        goto OUT;
    }

    destroy_vtysh();

OUT:
    return ret;
}
