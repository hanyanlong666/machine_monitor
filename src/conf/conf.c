/*
 * @Author: hanyanlong
 * @Date: 2024-03-18 15:36:05
 * @Description: 
 */
#include "../monitor_server/monitor_server.h"
#include "sys_lib.h"
#include "vtysh.h"

int read_config_from_file(char *file_path)
{
    FILE *fp = NULL;
    int ret = RET_SUCCESS;

    if(!file_path) {
        debug_exception("file_path is NULL!\n");
        return RET_ERR;
    }

    fp = fopen(file_path, "r");
    if(!fp) {
        debug_exception("Failed fopen: %s!\n", file_path);
        return RET_ERR;
    }

    ret = vtysh_in_from_file(fp);
    if(ret) {
        debug_exception("Failed in vtysh_in_from_file\n");
    }
    fclose(fp);

    return ret;
}


int conf_init()
{
    int ret;

    ret = read_config_from_file("aaa");
    if(ret) {
        debug_exception("Failed in vtysh_in_from_file\n");
        goto OUT;
    }

OUT:
    return ret;
}
