/*
 * @Autor: hanyanlong
 * @Date: 2023-02-27 11:18:50
 * @Description: 
 */
#include "sys_lib.h"

/**
 * @description: 获得自1970年1月1日0点以来经过的秒数
 * @return {*}
 */
time_t get_current_localtime_sec()
{
    time_t now;
    struct timeval tv;
    struct timezone tz;

	time(&now);
	gettimeofday(&tv, &tz);
	now += tz.tz_minuteswest * 60;

    return now;
}

/**
 * @description: 将时间秒数解析为年月日字符串
 * @param {time_t} now
 * @param {char} *time_str
 * @param {int} time_len
 * @return {*}
 */
void get_current_localtime_str(time_t now, char *time_str, int time_len)
{
    struct tm tm_time;

	gmtime_r(&now, &tm_time);
	memset(time_str, 0, time_len);
	sprintf(time_str, "%04d-%02d-%02d", tm_time.tm_year + 1900, tm_time.tm_mon + 1, tm_time.tm_mday);
}

/**
 * @description: 将当前时间秒数解析为年月日字符串 格式如"2022-12-01"
 * @param {char} *time_str
 * @param {int} time_len
 * @return {*}
 */
void get_current_localtime(char *time_str, int time_len)
{
    time_t now;
    now = get_current_localtime_sec();
    get_current_localtime_str(now, time_str, time_len);
}

/**
 * @description: 将 UTF-8 编码格式的文件转为 GB2312 编码格式文件
 * @param {char} *utf8_path
 * @param {char} *gb2312_path
 * @return {*}
 */
int utf8_to_gb2312(const char *utf8_path, const char *gb2312_path)
{
    char cmd[MAX_COMMAND_LEN];
    int ret;

    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "iconv -f UTF-8 -t GB2312 %s -o %s", utf8_path, gb2312_path);
    ret = system(cmd);
    if(ret) {
        debug_exception("Failed convert UTF-8 file [ %s ] to GB2312 file [ %s ]\n", utf8_path, gb2312_path);
        return RET_ERR;
    } else {
        debug_out("Successfully converted UTF-8 encoding file [%s] to GB2312 encoding file [%s]\n", utf8_path, gb2312_path);
    }

    return RET_SUCCESS;
}

/**
 * @description: 将 GB2312 编码格式的文件转为 UTF-8 编码格式文件
 * @param {char} *gb2312_path
 * @param {char} *utf8_path
 * @return {*}
 */
int gb2312_to_utf8(const char *gb2312_path, const char *utf8_path)
{
    char cmd[MAX_COMMAND_LEN];
    int ret;

    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "iconv -f GB2312 -t UTF-8 %s -o %s", gb2312_path, utf8_path);
    ret = system(cmd);
    if(ret) {
        debug_exception("Failed convert GB2312 file [ %s ] to UTF-8 file [ %s ]\n", gb2312_path, utf8_path);
    } else {
        debug_out("Successfully converted GB2312 encoding file [%s] to UTF-8 encoding file [%s]\n", gb2312_path, utf8_path);
    }

    return RET_SUCCESS;
}

/**
 * @description: 删除行首的空格以及行尾的换行符
 * @param {char} *str
 * @return {*}
 */
char *string_useless_del(char *str)
{
	int i = 0;
	char *ret = NULL;

	if(!str)
		return NULL;

	ret = str;
	while(i < strlen(str)) {
		if(*(str + i) == ' ' || *(str + i) == '\t')
			ret++;
		else
			break;
		i++;
	}

	if(ret[0] != '\n' && ret[strlen(ret) - 1] == '\n')
		ret[strlen(ret) - 1] = '\0';

	return ret;
}

/**
 * @description: 删除一行的前导和尾部空白
 * @param {char} *string
 * @return {*}
 */
char *stripwhite(char *string)
{
    register char *s, *t;

    for(s = string; whitespace(*s); s++);

    if (*s == 0)
        return (s);

    t = s + strlen (s) - 1;
    while((t > s) && whitespace(*t))
        t--;
    *++t = '\0';

    return s;
}

/**
 * @description: 检查字符串中是否包含中文
 * @param {char} *string
 * @return {*}
 */
int check_include_chinese(char *string)
{
	int len = strlen(string);
	int i = 0;

	for(i = 0; i < len; i++) {
		if((*(string + i) & 0x80))
			return RET_ERR;
    }

    return RET_SUCCESS;
}

/**
 * @description: 检查字符串是否为合法的数字
 * @param {char} *string
 * @return {*}
 */
int check_valid_digit(char *string)
{
    char *t;

    for(t = string; *t != '\0'; t++) {
        if(!isdigit(*t))
            return RET_ERR;
    }

    return RET_SUCCESS;
}
