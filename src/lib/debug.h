/*
 * @Author: hanyanlong
 * @Date: 2023-09-18 14:41:19
 * @Description: 
 */
#ifndef __DEBUG_H__
#define __DEBUG_H__

#define RET_SUCCESS 0
#define RET_ERR -1

// 标准输出
#define debug_out(fmt...) fprintf(stdout, fmt)

// 异常输出
// #define debug_errmsg(fmt, args...) fprintf(stderr, "  errmsg: "fmt, ## args)

#define debug_err(fmt...) fprintf(stderr, fmt)

#define debug_exception(fmt...)\
({\
    fprintf(stderr, "%s - %s - %d:  ", basename(__FILE__), __func__, __LINE__);\
    fprintf(stderr, fmt);\
    if(errno) {\
        fprintf(stderr, "  errmsg: %s\n", strerror(errno));\
        errno = 0;\
    }\
})

#endif