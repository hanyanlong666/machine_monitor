/*
 * @Author: hanyanlong
 * @Date: 2024-03-20 22:35:50
 * @Description: 
 */
#ifndef __CONST_H__
#define __CONST_H__

/* buffer length  */ 
#define MAX_ARRAY_ARGC		25
#define MAX_ARRAY_PARAM_LEN 32

#define MAX_IP_LEN 32
#define MAX_IP6_LEN 64
#define MAX_TIME_LEN 64
#define MAX_NAME_LEN 64
#define MAX_PATH_LEN 256
#define MAX_DIR_LEN MAX_PATH_LEN - MAX_NAME_LEN
#define MAX_ERRMSG_LEN 512
#define MAX_COMMAND_LEN 1024
#define MAX_DATA_LEN 2048

/* time unit 以1秒为基准 */
#define ONE_SECONE	1
#define HALF_MINUTE	ONE_SECONE * 30	// 30
#define ONE_MINUTE	ONE_SECONE * 60	// 60
#define HALF_HOUR	ONE_MINUTE * 30	// 1800
#define ONE_HOUR	ONE_MINUTE * 60	// 3600
#define HTREE_HOUR	ONE_HOUR * 3	// 10800
#define SIX_HOUR	ONE_HOUR * 6	// 21600
#define ONE_DAY		ONE_HOUR * 24	// 86400
#define ONE_WEEK    ONE_DAY * 7     // 604800
#define ONE_MONTH   ONE_DAY * 30    // 2592000

#endif