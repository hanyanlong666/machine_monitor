/*
 * @Autor: hanyanlong
 * @Date: 2023-04-13 11:15:38
 * @Description: 
 */
#ifndef __SYS_LIB_H__
#define __SYS_LIB_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stdint.h"
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <signal.h>
#include <ctype.h>
#include <libgen.h>
#include "const.h"
#include "debug.h"
#include "memory.h"
#include "linklist.h"
#include "split.h"

#ifndef likely
#define likely(x)	__builtin_expect((long)(!!(x)), (long)1)
#endif
#ifndef unlikely
#define unlikely(x)	__builtin_expect((long)(!!(x)), (long)0)
#endif

#ifndef whitespace
#define whitespace(c) (((c) == ' ') || ((c) == '\t'))
#endif

/* file and dir mode */
#define DIR_MODE            S_IRWXU
#define EXECUTE_FILE_MODE   S_IRWXU
#define REGULAR_FILE_MODE   (S_IRUSR | S_IWUSR)


struct message {
	int key;
	char *str;
};

static inline int sfwrite(FILE *fp, char *buf)
{
	int size;
	size = fwrite(buf, 1, strlen(buf), fp);
	if(size != strlen(buf)) {
		debug_exception("Failed write to file\n");
		return -1;
	}
	return 0;
}

/**************** function ****************/
#define CLEAR_BUFFER while(getchar() != '\n')

time_t get_current_localtime_sec();
void get_current_localtime_str(time_t now, char *time_str, int time_len);
void get_current_localtime(char *time_str, int time_len);
int utf8_to_gb2312(const char *utf8_path, const char *gb2312_path);
int gb2312_to_utf8(const char *gb2312_path, const char *utf8_path);
char *string_useless_del(char *str);
char *stripwhite(char *string);
int check_include_chinese(char *string);
int check_valid_digit(char *string);


#endif