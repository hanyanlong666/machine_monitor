/*
 * @Author: hanyanlong
 * @Date: 2024-03-20 22:41:16
 * @Description: 
 */
#ifndef __SPLIT_H__
#define __SPLIT_H__

// 分割字符串使用
struct split_array {
	char **array;
	int size;
	int max_size;
};

struct split_list {
	list list;
};

struct split_array *split_array_new();
void split_array_delete(struct split_array *sa);
void split_array_reset(struct split_array *sa);
int split_array_node_insert(struct split_array *sa, char *string);
int split_line_to_array(struct split_array *sa, char *line, char *split_str);
void split_array_loop_print(struct split_array *sa);

struct split_list *split_list_new();
void split_list_delete(struct split_list *sl);
void split_list_reset(struct split_list *sl);
int split_list_node_insert(struct split_list *sl, char *string);
int split_line_to_list(struct split_list *sl, char *line, char *split_str);
void split_list_loop_print(struct split_list *sl);

#endif