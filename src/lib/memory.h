/*
 * @Author: hanyanlong
 * @Date: 2024-03-22 10:26:56
 * @Description: 
 */
#ifndef __MEMORY_H__
#define __MEMORY_H__

enum memory_type {
    MTYPE_TMP = 1,
    MTYPE_LINK_LIST,
    MTYPE_LINK_NODE,
	MTYPE_SPLIT,
	MTYPE_SPLIT_MEMBER,

	MTYPE_VTYSH_ELEMENT,
	MTYPE_VTYSH_NODE,
	MTYPE_VTYSH_VECTOR,
	MTYPE_VTYSH_VTY,

	MTYPE_HM,

	MTYPE_LOG,

    MTYPE_MAX,
};

void *zmalloc(int type, size_t size);
void  zfree(int type, void *ptr);

#define XMALLOC(mtype, size)    zmalloc((mtype), (size))
#define XFREE(mtype, ptr)       zfree((mtype), (ptr))

#endif