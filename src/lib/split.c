/*
 * @Author: hanyanlong
 * @Date: 2024-03-20 22:41:11
 * @Description: 
 */
#include "sys_lib.h"

struct split_array *split_array_new(int max_size)
{
    struct split_array *sa;
    int i;

    sa = malloc(sizeof(struct split_array));
    memset(sa, 0, sizeof(struct split_array));
    if(!sa) {
        debug_exception("Failed malloc split_array\n");
        return NULL;
    }

    sa->array = malloc(sizeof(char *) * max_size);
    if(!sa->array) {
        debug_exception("Failed malloc sa->array\n");
        free(sa);
        return NULL;
    }
    // for(i = 0; i < sa)
    

    return sa;
}

void split_array_delete(struct split_array *sa)
{
    if(!sa)
        return;

    for(int i = 0; i < MAX_ARRAY_ARGC; i++){
        if(sa->array[i])
            free(sa->array[i]);
    }
    free(sa);
}

/**
 * @description: 重置split_array
 * @param {split_array} *sa
 * @param {int} release
 * @return {*}
 */
void split_array_reset(struct split_array *sa)
{
    if(!sa)
        return;

    for(int i = 0; i < MAX_ARRAY_ARGC; i++){
        if(sa->array[i]) {
            free(sa->array[i]);
            sa->array[i] = NULL;
        }
    }
    sa->size = 0;
}

int split_array_node_insert(struct split_array *sa, char *string)
{
    if(!sa) {
        debug_err("Split array is NULL.\n");
        return RET_ERR;
    }

    if(sa->size >= MAX_ARRAY_ARGC) {
        debug_err("Too much option.\n");
        return RET_ERR;
    }

    if(strlen(string) >= MAX_ARRAY_PARAM_LEN) {
        debug_err("Command option too long.\n");
        return RET_ERR;
    }

    sa->array[sa->size] = strdup(string);
    sa->size++;

    return RET_SUCCESS;
}

/**
 * @description: 按照指定分隔符分割字符串到split_array中
 * @param {split_array} *sa
 * @param {char} *line
 * @param {char} *split_str
 * @return {*}
 */
int split_line_to_array(struct split_array *sa, char *line, char *split_str)
{
    char buf[MAX_DATA_LEN];
    char *string;

    if(!sa) {
        debug_err("Split array is NULL\n");
        return RET_ERR;
    }

    memset(buf, 0, sizeof(buf));
    strcpy(buf, line);

    for(string = strtok(buf, split_str); string; string = strtok(NULL, split_str)) {
        if(split_array_node_insert(sa, stripwhite(string)))
            return RET_ERR;
    }

    return RET_SUCCESS;
}

void split_array_loop_print(struct split_array *sa)
{
    if(!sa || !sa->size)
        return;

    for(int i = 0; i < MAX_ARRAY_ARGC; i++){
        if(sa->array[i]) {
            debug_out("count = %d\n", sa->size);
            debug_out("array[%d] = %s\n", i, sa->array[i]);
        }
    }
}

struct split_list *split_list_new()
{
    struct split_list *sl;

    sl = malloc(sizeof(struct split_list));
    memset(sl, 0, sizeof(struct split_list));
    if(!sl) {
        debug_exception("Failed malloc split_list\n");
        return NULL;
    }
    sl->list = list_new();

    return sl;
}

/**
 * @description: 释放分割好的字符串链表
 * @param {list} list
 * @return {*}
 */
void split_list_delete(struct split_list *sl)
{
    listnode nn;
    char *del_str;

    if(!sl || !sl->list)
        return;

    LIST_LOOP(sl->list, del_str, nn)
        free(del_str);
    list_delete(sl->list);
    free(sl);
}

/**
 * @description: 重置split_array
 * @param {split_array} *sa
 * @param {int} release
 * @return {*}
 */
void split_list_reset(struct split_list *sl)
{
    listnode nn;
    char *del_str;

    if(!sl || sl->list)
        return;

    if(list_isempty(sl->list))
        return;

    LIST_LOOP(sl->list, del_str, nn)
        free(del_str);
    list_delete_all_node(sl->list);
}

int split_list_node_insert(struct split_list *sl, char *string)
{
    if(!sl || !sl->list) {
        debug_err("Split list is NULL.\n");
        return RET_ERR;
    }

    char *member = strdup(string);
    listnode_add(sl->list, member);

    return RET_SUCCESS;
}

/**
 * @description: 按照指定分隔符分割字符串到链表中
 * @param {char} *line
 * @param {char} *split_str
 * @param {list} list
 * @return {*}
 */
int split_line_to_list(struct split_list *sl, char *line, char *split_str)
{
    char buf[MAX_DATA_LEN];
	char *string;

    if(!sl || !sl->list) {
        debug_err("Split list is NULL.\n");
        return RET_ERR;
    }

    memset(buf, 0, sizeof(buf));
    strcpy(buf, line);

	for(string = strtok(buf, split_str); string != NULL; string = strtok(NULL, split_str)) {
        if(split_list_node_insert(sl, stripwhite(string)))
            return RET_ERR;
	}

    return RET_SUCCESS;
}

void split_list_loop_print(struct split_list *sl)
{
    listnode nn;
    char *str;
    int i = 0;

    if(!sl || !sl->list)
        return;

    if(list_isempty(sl->list))
        return;

    LIST_LOOP(sl->list, str, nn) {
        debug_out("count = %d\n", listcount(sl->list));
        debug_out("list[%d] = %s\n", i, str);
    }
}
