/*
 * @Author: hanyanlong
 * @Date: 2024-03-22 10:26:23
 * @Description: 
 */
#include "sys_lib.h"

struct memstat
{
    char *name;
    unsigned long alloc;
    unsigned long diff;
} mstat[MTYPE_MAX];

struct message mstr[] =
{
    { MTYPE_TMP,        "temp"      },
    { MTYPE_LINK_LIST,  "linklist"  },
    { MTYPE_LINK_NODE,  "linknode"  },
    { 0, NULL },
};

/* Message lookup function. */
char *lookup (struct message *mes, int key)
{
    struct message *pnt;

    for (pnt = mes; pnt->key != 0; pnt++) 
    if (pnt->key == key) 
        return pnt->str;

  return "";
}

/* Increment allocation counter. */
void alloc_inc(int type)
{
    mstat[type].alloc++;
}

/* Decrement allocation counter. */
void alloc_dec(int type)
{
    mstat[type].alloc--;
}

/* Fatal memory allocation error occured. */
static void zerror(const char *fname, int type, size_t size)
{
	fprintf(stderr, "%s : can't allocate memory for '%s' size %d\n", fname, lookup (mstr, type), (int) size);
	exit(1);
}

/* Memory allocation. */
void *zmalloc(int type, size_t size)
{
    void *memory;

    memory = malloc(size);

    if(memory == NULL)
        zerror("malloc", type, size);

    alloc_inc(type);

    return memory;
}

/* Memory free. */
void zfree(int type, void *ptr)
{
    if(ptr)
        alloc_dec(type);

    free(ptr);
}
