/*
 * @Author: hanyanlong
 * @Date: 2024-03-25 15:31:59
 * @Description: 
 */
#ifndef __NET_INCLUDES_H__
#define __NET_INCLUDES_H__

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <netinet/icmp6.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <linux/sockios.h>

#define IP_HEADER_LEN   sizeof(struct ip)
#define TCP_HEADER_LEN  sizeof(struct tcphdr)
#define UDP_HEADER_LEN  sizeof(struct udphdr) 
#define ICMP_HEADER_LEN sizeof(struct icmphdr)

#define TCP_DATA_LEN (ETH_DATA_LEN - IP_HEADER_LEN - TCP_HEADER_LEN)
#define UDP_DATA_LEN (ETH_DATA_LEN - IP_HEADER_LEN - UDP_HEADER_LEN)

#define NIPQUAD(addr) \
	((unsigned char *)&addr)[0], \
	((unsigned char *)&addr)[1], \
	((unsigned char *)&addr)[2], \
	((unsigned char *)&addr)[3]

#define NIPQUAD_FMT "%u.%u.%u.%u"

#define NIP6QUAD(addr) \
     ((unsigned char *)&addr)[0], \
     ((unsigned char *)&addr)[1], \
     ((unsigned char *)&addr)[2], \
     ((unsigned char *)&addr)[3], \
     ((unsigned char *)&addr)[4], \
     ((unsigned char *)&addr)[5], \
     ((unsigned char *)&addr)[6], \
     ((unsigned char *)&addr)[7], \
     ((unsigned char *)&addr)[8], \
     ((unsigned char *)&addr)[9], \
     ((unsigned char *)&addr)[10], \
     ((unsigned char *)&addr)[11], \
     ((unsigned char *)&addr)[12], \
     ((unsigned char *)&addr)[13], \
     ((unsigned char *)&addr)[14], \
     ((unsigned char *)&addr)[15]

#define NIP6QUAD_FMT "%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x"

#endif