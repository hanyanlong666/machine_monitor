/*
 * @Author: hanyanlong
 * @Date: 2024-03-27 14:02:31
 * @Description: 
 */
#include "sys_lib.h"
#include "net_includes.h"
#include "monitor_object.h"
#include "object_vty.h"

static inline struct monitor_obj *monitor_obj_new()
{
    struct monitor_obj *mobj;
    mobj = malloc(sizeof(struct monitor_obj));
    memset(mobj, 0, sizeof(struct monitor_obj));
    if(!mobj) {
        debug_exception("Failed malloc monitor_obj\n");
        return NULL;
    }
    return mobj;
}

static inline void monitor_obj_delete(struct monitor_obj *mobj)
{
    if(!mobj)
        return;
    free(mobj);
}

