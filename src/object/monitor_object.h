/*
 * @Author: hanyanlong
 * @Date: 2024-03-26 16:28:13
 * @Description: 
 */
#ifndef __MONITOR_OBJECT_H__
#define __MONITOR_OBJECT_H__

struct monitor_obj {
    char name[MAX_NAME_LEN];
    int ip_type;
    unsigned int ip;
    unsigned int ip6[4];
    list hm_list;
};


#endif